(function ($) {
  Drupal.behaviors.ajaxAjaxPagerBehavior = {
    attach: function (context, settings) {
      if (drupalSettings.ajax_pager) {

        for (pager_id in drupalSettings.ajax_pager) {
          // Add ajax-processed Class so module ajax_comments will ignore.
          $('*[data-ajax_pager="' + pager_id + '"]' +
            ' .pager a:not(.ajax-pager-processed)'
          ).addClass(
            'ajax-pager-processed').addClass('ajax-processed').each(
            function () {
              console.log('Process pager:' + pager_id);
              $(this).attr(
                'onclick',
                'ajax_pager(this, \'' +
                pager_id +
                '\'); return false;'
              );
            }
          );
        }
      }
    }
  }

  window.ajax_pager = function (obj, pager_id) {
    var queryString = $(obj).attr('href').substring(
      $(obj).attr('href').indexOf('?') + 1
    );
    Drupal.ajax({
      url: drupalSettings.ajax_pager[pager_id].ajax_url + '?' + queryString,
      submit: drupalSettings.ajax_pager[pager_id],
      element: obj,
      progress: {
        type: 'throbber'
      }
    }).execute();

    return FALSE;
  }

  Drupal.AjaxCommands.prototype.AjaxPagerScrollTop = function (ajax, response) {
    var offset = $(response.selector).offset();

    var scrollTarget = response.selector;
    while ($(scrollTarget).scrollTop() === 0 && $(scrollTarget).parent()) {
      scrollTarget = $(scrollTarget).parent();
    }

    if (offset.top - 10 < $(scrollTarget).scrollTop()) {
      $(scrollTarget).animate({
        scrollTop: offset.top - 10
      }, 500);
    }
  };

})(jQuery);
