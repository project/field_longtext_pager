<?php

namespace Drupal\field_longtext_pager\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Ajax page request manager.
 *
 * A controller to help track dynamically generated pager ids and their
 * corresponding fields across HTTP requests.
 *
 * @package Drupal\field_longtext_pager
 */
class AjaxManager extends ControllerBase {

  /**
   * The PrivateTempStore service.
   *
   * This service stores temporary data to be used across HTTP requests.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStore;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * AjaxManager constructor.
   *
   * @param \Drupal\user\PrivateTempStoreFactory $temp_store_factory
   *   The factory to create the PrivateTempStore object.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The requestStack object.
   */
  final public function __construct(
    PrivateTempStoreFactory $temp_store_factory,
    RequestStack $requestStack
  ) {
    $this->tempStore = $temp_store_factory->get('field_longtext_pager');
    $this->requestStack = $requestStack;

    // Pager.counter tracks assigned pager ids;.
    if (empty($this->tempStore->get('pager.counter'))) {
      $this->tempStore->set('pager.counter', 0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
      $container->get('request_stack')
    );
  }

  /**
   * Stores the Get request page query accross HTTP requests.
   */
  public function setPageQuery($pager_index) {

    $pager_query = implode(',', $pager_index);
    $this->tempStore->set('pager.index', $pager_query);

  }

  /**
   * Gets the page request index.
   *
   * @return string
   *   Returns page query parameter from get requests across HTTP requests.
   */
  public function getPageQuery() {

    // Retrieve page Get request object.
    $request = $this->requestStack->getCurrentRequest();

    // Retrieve page query parameter from Get request object.
    $page_request = $request->get('page') ? $request->get('page') : '0';

    // Explode page request into array for processing.
    $page_request_index = explode(',', $page_request);

    // Sanitize null values to 0.
    foreach ($page_request_index as $index) {
      if (!empty($index) && is_numeric($index) && $index >= 0) {
        $pager_index[] = $index;
      }
      else {
        $pager_index[] = 0;
      }
    }
    $pager_index = implode(',', $pager_index);

    return($pager_index);
  }

}
