<?php

namespace Drupal\field_longtext_pager\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FieldLongtextPagerSettingsForm.
 *
 * Provides the default module settings form.
 *
 * @package Drupal\field_longtext_pager\Form
 */
class FieldLongtextPagerSettingsForm extends FormBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * A list of the default field pager formatter settings.
   *
   * @var array
   */
  private $settings = [
    'paging_method',
    'pagebreak',
    'character_count',
    'word_count',
    'pager_position',
    'index_position',
    'index_prefix',
    'index_postfix',
    'read_time_position',
    'words_per_minute',
    'ajax_pager',
  ];

  /**
   * Construct a FieldLongtextPagerSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  final public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'field_longtext_pager_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    foreach ($this->settings as $field_name) {
      $settings[$field_name] = $this->configFactory->get('field_longtext_pager.settings')
        ->get($field_name);
    }

    $form = [
      '#type' => 'fieldset',
      '#title' => $this->t('Field longtext pager default values'),
      '#description' => $this->t('These values will set the default values for all fields usinthe field pager display formatter. These values can be customised during content editing.'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];

    $form['paging_method'] = [
      '#type' => 'select',
      '#title' => $this->t('Page break method'),
      '#default_value' => $settings['paging_method'],
      '#options' => field_longtext_pager_method_options(),
    ];

    $form['pagebreak'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Page break placeholder'),
      '#default_value' => $settings['pagebreak'],
      '#description' => $this->t('HTML comment or valid HTML tag with unique identifier, eg. &lt;hr class="pagebreak" /&gt; or &lt;!--pagebreak--&gt.'),
      '#size' => 50,
    ];

    $form['character_count'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Character limit'),
      '#description' => $this->t('Number of characters that will be shown for each page when "Automatic page break by character limit" is selected.'),
      '#default_value' => $settings['character_count'],
      '#size' => 50,
    ];

    $form['word_count'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Word limit'),
      '#description' => $this->t('Number of words that will be shown for each page when "Automatic page break by word limit" is selected.'),
      '#default_value' => $settings['word_count'],
      '#size' => 50,
    ];

    // Fetch options for position setting select boxes.
    $position_options = field_longtext_pager_position_options();

    // Pager position setting form field.
    $pager_position_setting_id = $settings['pager_position'];
    $position_options[$pager_position_setting_id];
    $form['pager_position'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Position'),
      '#description' => $this->t('Position of pager relative to field.'),
      '#default_value' => $pager_position_setting_id,
      '#options'       => $position_options,
    ];

    // Page count and index position setting form field.
    $index_position_setting_id = $settings['index_position'];
    $position_options[$index_position_setting_id];
    $form['index_position'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Index'),
      '#description' => $this->t('Position of page index relative to field.'),
      '#default_value' => $index_position_setting_id,
      '#options'       => $position_options,
    ];

    $form['index_prefix'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Index prefix'),
      '#description' => $this->t('Text to appear before the page index.'),
      '#default_value' => $settings['index_prefix'],
    ];

    $form['index_postfix'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Index postfix'),
      '#description' => $this->t('Text to appear after the page index.'),
      '#default_value' => $settings['index_postfix'],
    ];

    // Read time setting form field.
    $read_time_position_setting_id = $settings['read_time_position'];
    $position_options[$read_time_position_setting_id];
    $form['read_time_position'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Reading time'),
      '#description' => $this->t('Position of estimated reading time relative to field.'),
      '#default_value' => $read_time_position_setting_id,
      '#options'       => $position_options,
    ];

    $form['words_per_minute'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Words per minute'),
      '#description' => $this->t('The number of words per minute to calculate the average read time statistic.'),
      '#default_value' => $settings['words_per_minute'],
      '#size' => 50,
    ];

    $form['ajax_pager'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ajax pager'),
      '#default_value' => $settings['ajax_pager'],
      '#description' => $this->t('Enable ajax pager.'),
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this
        ->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $pagebreak = $form_state->getValue('pagebreak');
    $str_length = mb_strlen($form_state->getValue('pagebreak')) - 1;

    if (strpos($pagebreak, '<') === FALSE || strpos($pagebreak, '>') != $str_length
    ) {
      $form_state->setError($form['pagebreak'],
      $this->t('Page break placeholder is not a valid HTML tag or comment.'));
    }

    if (!is_numeric($form_state->getValue('character_count'))) {
      $form_state->setError($form['character_count'],
      $this->t('Character limit should be numeric value.'));
    }

    if (!is_numeric($form_state->getValue('word_count'))) {
      $form_state->setError($form['word_count'],
      $this->t('Word limit should be numeric value.'));
    }

    if (!is_numeric($form_state->getValue('words_per_minute'))) {
      $form_state->setError($form['words_per_minute'],
      $this->t('Words per minute should be numeric value.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->configFactory->getEditable('field_longtext_pager.settings');

    foreach ($this->settings as $field_name) {
      $value = $form_state->getValue($field_name);
      $config->set($field_name, $value)->save();
    }

  }

}
