<?php

namespace Drupal\field_longtext_pager\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * AJAX command for calling the ajaxPagerScrollTopCommand() method.
 *
 * @ingroup ajax
 */
class AjaxPagerScrollTopCommand implements CommandInterface {

  /**
   * The CSS selector for the element.
   *
   * @var string
   */
  protected $selector;

  /**
   * Constructs a ajaxPagerScrollToElementCommand object.
   *
   * @param string $selector
   *   The CSS selector of the ajax paged field block.
   */
  public function __construct($selector) {
    $this->selector = $selector;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'AjaxPagerScrollTop',
      'selector' => $this->selector,
    ];
  }

}
