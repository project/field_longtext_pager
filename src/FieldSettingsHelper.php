<?php

namespace Drupal\field_longtext_pager;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FieldSettingsHelper.
 *
 * Provides helper functions for returning field settings.
 *
 * @package Drupal\field_longtext_pager
 */
class FieldSettingsHelper {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The field formatter plugin manager service.
   *
   * @var \Drupal\Core\Field\FormatterPluginManager
   */
  protected $fieldFormatterManager;

  /**
   * FieldSettingsHelper constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  final public function __construct(
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Get the entity view display configuration for a specified field.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The entity bundle.
   * @param string $field_name
   *   The name of the field to get the view display for.
   * @param string $view_mode
   *   The current view mode.
   *
   * @return \Drupal\Core\Entity\Display\EntityDisplayInterface
   *   The entity view display configuration for the commented entity.
   */
  public function getEntityViewDisplaySettings(
    $entity_type,
    $bundle,
    $field_name,
    $view_mode = 'default'
  ) {

    // Try to load the configuration entity for the entity's
    // view display settings.
    /** @var \Drupal\Core\Entity\Display\EntityDisplayInterface $view_display */
    $view_display = $this->entityTypeManager
      ->getStorage('entity_view_display')
      ->load("$entity_type.$bundle.$view_mode");

    // If there is no entity view display configuration for the provided
    // view mode, fall back on the default view mode.
    if (empty($view_display)) {
      $view_display = $this->entityTypeManager
        ->getStorage('entity_view_display')
        ->load("$entity_type.$bundle.default");
    }

    if (!empty($view_display)) {
      // Get field display settings from entity display.
      $display_settings = $view_display->getComponent($field_name);

      return $display_settings;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Returns enable ajax pager setting.
   */
  public function ajaxEnabled(
    $entity_type,
    $bundle,
    $field_name,
    $view_mode = 'default'
  ) {

    $display_settings = $this->getEntityViewDisplaySettings(
    $entity_type, $bundle, $field_name, $view_mode = 'default');

    if (isset($display_settings['settings']['ajax_pager'])) {
      return $display_settings['settings']['ajax_pager'];
    }
    else {
      return FALSE;
    }
  }

  /**
   * Returns pager id setting.
   */
  public function getPagerId($entity_type, $bundle, $field_name, $view_mode = 'default') {

    $display_settings = $this->getEntityViewDisplaySettings(
    $entity_type, $bundle, $field_name, $view_mode);

    if (isset($display_settings['settings']['pager_id'])) {
      return $display_settings['settings']['pager_id'];
    }
    else {
      return FALSE;
    }
  }

}
