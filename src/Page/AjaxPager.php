<?php

namespace Drupal\field_longtext_pager\Page;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\field_longtext_pager\Ajax\AjaxPagerScrollTopCommand;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns requested subpage content for paged field ajax requests.
 */
class AjaxPager extends ControllerBase {

  /**
   * Returns requested subpage content for paged field ajax requests.
   */
  public static function getSubpage(Request $request) {

    $response = new AjaxResponse();

    $ajax_pager = $request->request->get('ajax_pager');

    // Get pager and field data from post request.
    $entity_id = $ajax_pager['entity_id'];
    $entity_type = $ajax_pager['entity_type'];
    $bundle = $ajax_pager['bundle'];
    $field_name = $ajax_pager['field_name'];
    $view_mode = $ajax_pager['view_mode'];

    // Load entity to obtain field access and display data.
    $entity = \Drupal::entityTypeManager()->getStorage($entity_type)
      ->load($entity_id);

    // If field exists and is viewable.
    if ($entity->hasField($field_name) && $entity->access('view')) {

      // Get field content.
      $field_content = $entity->get($field_name);

      // Load the field display settings helper service.
      $field_settings_helper = \Drupal::service(
        'field_longtext_pager.field_settings_helper');

      // Fetch pager_id for field.
      $pager_id = $field_settings_helper->getPagerId($entity_type, $bundle, $field_name);

      // Get display settings from helper.
      $display_settings = $field_settings_helper->getEntityViewDisplaySettings(
        $entity_type, $bundle, $field_name, $view_mode);

      // Get view builder for entity type.
      $viewBuilder = \Drupal::entityTypeManager()->getViewBuilder($entity_type);

      // Build paged field content output using viewbuilder.
      $output = $viewBuilder->viewField($field_content, $display_settings);

      // On click, update field with requested subpage content using Ajax.
      $selector = "*[data-ajax_pager=\"$pager_id\"]";

      // Ajax replace field content with requested subpage output.
      $response->addCommand(new ReplaceCommand($selector, $output));

      // Ajax scroll browser window focus to top of paged field.
      $response->addCommand(new AjaxPagerScrollTopCommand($selector));

    }

    return $response;
  }

}
