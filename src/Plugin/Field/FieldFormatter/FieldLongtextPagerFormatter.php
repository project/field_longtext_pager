<?php

namespace Drupal\field_longtext_pager\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\field_longtext_pager\Controller\AjaxManager;
use Drupal\filter\FilterProcessResult;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Plugin implementation of the field paging formatter.
 *
 * @FieldFormatter(
 *   id = "longtext_pager",
 *   label = @Translation("Field pager"),
 *   description = @Translation("Adds ajax paging to field content."),
 *   field_types = {
 *     "text_long",
 *     "text_with_summary",
 *   },
 *   quickedit = {
 *     "editor" = "form"
 *   }
 * )
 */
class FieldLongtextPagerFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The route manager service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $routeMatch;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The Ajax pager temporary store.
   *
   * @var \Drupal\field_longtext_pager\Controller\AjaxManager
   */
  protected $ajaxManager;

  /**
   * The view mode of the current formatter display.
   *
   * @var string
   */
  protected $viewMode;

  /**
   * The pager manager service.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * A list of the field pager formatter settings.
   *
   * @var const array
   */
  protected const SETTINGS = [
    'pagebreak',
    'paging_method',
    'character_count',
    'word_count',
    'block_count',
    'pager_position',
    'index_position',
    'index_prefix',
    'index_postfix',
    'read_time_position',
    'words_per_minute',
    'ajax_pager',
    'pager_id',
  ];

  /**
   * Construct a FieldLongtextPagerFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Defines an interface for entity field definitions.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity manager service.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $route_match
   *   The route match service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack service.
   * @param \Drupal\field_longtext_pager\Controller\AjaxManager $ajaxManager
   *   The ajax manager service.
   * @param \Drupal\Core\Pager\PagerManagerInterface $pagerManager
   *   The ajax manager service.
   */
  final public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    EntityTypeManagerInterface $entityTypeManager,
    CurrentRouteMatch $route_match,
    RequestStack $requestStack,
    AjaxManager $ajaxManager,
    PagerManagerInterface $pagerManager
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings
    );
    $this->entityTypeManager = $entityTypeManager;
    $this->routeMatch = $route_match;
    $this->requestStack = $requestStack;
    $this->ajaxManager = $ajaxManager;
    $this->viewMode = $view_mode;
    $this->pagerManager = $pagerManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],

      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get('request_stack'),
      $container->get('field_longtext_pager.ajax_manager'),
      $container->get('pager.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $default_settings = self::SETTINGS;
    foreach ($default_settings as $setting) {
      $settings[$setting] = \Drupal::config('field_longtext_pager.settings')
        ->get($setting);
    }
    return $settings + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {

    $summary = parent::settingsSummary();
    $paging_method = $this->getSetting('paging_method');

    switch ($paging_method) {
      default:
      case 'BYPASS':
        $label = $this->t('Bypass');
        $details = $this->t('Field paging is disabled.');
        break;

      case 'PLACEHOLDER':
        $label = $this->t('Placeholder');
        $details = $this->t(
          'Page break placeholder: @element',
          ['@element' => $this->getSetting('pagebreak')]
        );
        break;

      case 'CHARACTER':
        $label = $this->t('Character');
        $details = $this->t(
          'Character limit: @element',
          ['@element' => $this->getSetting('character_count')]
        );
        break;

      case 'WORD':
        $label = $this->t('Word');
        $details = $this->t(
          'Word limit: @element',
          ['@element' => $this->getSetting('word_count')]
        );
        break;

      case 'WORD_BLOCK':
        $label = $this->t('Word block');
        $details[] = $this->t(
          'Word limit: @element',
          ['@element' => $this->getSetting('word_count')]
        );
        $details[] = $this->t(
          'Block limit: @element',
          ['@element' => $this->getSetting('block_count')]
        );
        break;
    }

    $summary[] = $this->t(
      'Method: @element',
      ['@element' => $label]
    );
    foreach ($details as $detail) {
      $summary[] = $detail;
    }

    if (!empty($paging_method) && $paging_method != 'BYPASS') {

      // Fetch options for position-settings select boxes.
      $position_options = field_longtext_pager_position_options();

      // Pager field position setting.
      $pager_position_setting_id = $this->getSetting('pager_position');
      $pager_position_setting_label
        = $position_options[$pager_position_setting_id];

      $summary[] = $this->t(
        'Pager position: @element',
        ['@element' => $pager_position_setting_label]
      );

      // Page index and count field position settings.
      $index_position_setting_id = $this->getSetting('index_position');
      $index_position_setting_label
        = $position_options[$index_position_setting_id];

      $summary[] = $this->t(
        'Index position: @element',
        ['@element' => $index_position_setting_label]
      );

      if ($index_position_setting_id != 'HIDDEN') {
        $prefix = $this->getSetting('index_prefix');

        if (!empty($prefix)) {
          $summary[] = $this->t(
            'Index prefix: @element',
            ['@element' => $this->getSetting('index_prefix')]
          );
        }

        $prefix = $this->getSetting('index_postfix');
        if (!empty($postfix)) {
          $summary[] = $this->t(
            'Index postfix: @element',
            ['@element' => $this->getSetting('index_postfix')]
          );
        }
      }

      // Average read time statistic position.
      $read_time_position_setting_id = $this->getSetting('read_time_position');
      $read_time_position_setting_label
        = $position_options[$read_time_position_setting_id];

      $summary[] = $this->t(
        'Read time: @element',
        ['@element' => $read_time_position_setting_label]
      );

      if ($read_time_position_setting_id != 'HIDDEN') {
        $summary[] = $this->t(
          'Words per minute: @element',
          ['@element' => $this->getSetting('words_per_minute')]
        );
      }

      $ajax_pager = $this->getSetting('ajax_pager') ? 'On' : 'Off';
      $summary[] = $this->t(
        'Ajax pager: @element',
        ['@element' => $ajax_pager]
      );

      $pager_id = $this->getSetting('pager_id') ? $this->getSetting('pager_id') : '0';
      $summary[] = $this->t(
        'Pager id: @element',
        ['@element' => $pager_id]
      );
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $form = parent::settingsForm($form, $form_state);
    $paging_method_options = field_longtext_pager_method_options();

    $form['paging_method'] = [
      '#type' => 'select',
      '#title' => $this->t('Page break method'),
      '#default_value' => $this->getSetting('paging_method'),
      '#options' => $paging_method_options,
      '#attributes' => [
        'state-id' => 'method',
      ],
    ];

    $form['pagebreak'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Page break placeholder'),
      '#default_value' => $this->getSetting('pagebreak') ? $this->getSetting('pagebreak') : '<hr class="pagebreak">',
      '#description' => $this->t('HTML comment or valid HTML tag with unique identifier, eg. &lt;hr class="pagebreak" /&gt;.'),
      '#size' => 50,
      '#states' => [
        'visible' => [
          ':input[state-id="method"]' => [
            'value' => 'PLACEHOLDER',
            'enabled' => TRUE,
          ],
        ],
      ],
    ];

    $form['character_count'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Character limit'),
      '#description' => $this->t('Number of characters that will be shown for each page when "Automatic page break by character limit" is selected.'),
      '#default_value' => $this->getSetting('character_count') ? $this->getSetting('character_count') : 512,
      '#size' => 50,
      '#states' => [
        'visible' => [
          ':input[state-id="method"]' => [
            'enabled' => TRUE,
            'value' => 'CHARACTER',
          ],
        ],
      ],
    ];

    $form['word_count'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Word limit'),
      '#description' => $this->t('Number of words that will be shown for each page when "Automatic page break by word limit or block word limit" is selected.'),
      '#default_value' => $this->getSetting('word_count') ? $this->getSetting('word_count') : 128,
      '#size' => 50,
      '#states' => [
        'visible' => [
          [':input[state-id="method"]' => ['value' => 'WORD']],
          [':input[state-id="method"]' => ['value' => 'WORD_BLOCK']],
        ],
      ],
    ];

    $form['block_count'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Block limit'),
      '#description' => $this->t('Number of html blocks that will be shown for each page when "Automatic page break by block or block word limit" is selected.'),
      '#default_value' => $this->getSetting('block_count') ? $this->getSetting('block_count') : 5,
      '#size' => 50,
      '#states' => [
        'visible' => [
          [':input[state-id="method"]' => ['value' => 'BLOCK']],
          [':input[state-id="method"]' => ['value' => 'WORD_BLOCK']],
        ],
      ],
    ];

    // Fetch options for position setting select boxes.
    $position_options = field_longtext_pager_position_options();

    // Pager position setting form field.
    $pager_position_setting_id = $this->getSetting('pager_position');
    $position_options[$pager_position_setting_id];
    $form['pager_position'] = [
      '#type' => 'select',
      '#title' => $this->t('Pager position'),
      '#description' => $this->t('Position of pager relative to field.'),
      '#default_value' => $pager_position_setting_id,
      '#options' => $position_options,
      '#states' => [
        'invisible' => [
          ':input[state-id="method"]' => [
            'value' => 'BYPASS',
          ],
        ],
      ],
    ];

    // Page count and index position setting form field.
    $index_position_setting_id = $this->getSetting('index_position');
    $position_options[$index_position_setting_id];
    $form['index_position'] = [
      '#type' => 'select',
      '#title' => $this->t('Page index position'),
      '#description' => $this->t('Position of page index relative to field.'),
      '#default_value' => $index_position_setting_id,
      '#options' => $position_options,
      '#attributes' => [
        'state-id' => 'index-position',
      ],
      '#states' => [
        'invisible' => [':input[state-id="method"]' => ['value' => 'BYPASS']],
      ],
    ];

    $form['index_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Index prefix'),
      '#description' => $this->t('Text to appear before the page index.'),
      '#default_value' => $this->getSetting('index_prefix'),
      '#states' => [
        'invisible' => [
          [':input[state-id="method"]' => ['value' => 'BYPASS']],
          [':input[state-id="index-position"]' => ['value' => 'HIDDEN']],
        ],
      ],
    ];

    $form['index_postfix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Index postfix'),
      '#description' => $this->t('Text to appear after the page index.'),
      '#default_value' => $this->getSetting('index_postfix'),
      '#states' => [
        'invisible' => [
          [':input[state-id="method"]' => ['value' => 'BYPASS']],
          [':input[state-id="index-position"]' => ['value' => 'HIDDEN']],
        ],
      ],
    ];

    // Read time setting form field.
    $read_time_position_setting_id = $this->getSetting('read_time_position');
    $position_options[$read_time_position_setting_id];
    $form['read_time_position'] = [
      '#type' => 'select',
      '#title' => $this->t('Reading time position'),
      '#description' => $this->t('Position of estimated reading time relative to field.'),
      '#default_value' => $read_time_position_setting_id,
      '#options' => $position_options,
      '#attributes' => [
        'state-id' => 'read-time-position',
      ],
      '#states' => [
        'invisible' => [':input[state-id="method"]' => ['value' => 'BYPASS']],
      ],
    ];

    $form['words_per_minute'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Words per minute'),
      '#description' => $this->t('The number of words per minute to calculate the average reading time statistic.'),
      '#default_value' => $this->getSetting('words_per_minute') ? $this->getSetting('words_per_minute') : 225,
      '#size' => 50,
      '#states' => [
        'invisible' => [
          [':input[state-id="method"]' => ['value' => 'BYPASS']],
          [':input[state-id="read-time-position"]' => ['value' => 'HIDDEN']],
        ],
      ],
    ];

    $form['ajax_pager'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ajax pager'),
      '#default_value' => $this->settings['ajax_pager'] ? $this->settings['ajax_pager'] : TRUE,
      '#description' => $this->t('Enable ajax pager.'),
      '#states' => [
        'invisible' => [':input[state-id="method"]' => ['value' => 'BYPASS']],
      ],
    ];

    $form['pager_id'] = [
      '#type' => 'number',
      '#title' => $this->t('Pager id'),
      '#default_value' => $this->getSetting('pager_id') ? $this->getSetting('pager_id') : 0,
      '#states' => [
        'invisible' => [':input[state-id="method"]' => ['value' => 'BYPASS']],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    // Get pager settings.
    $paging_method = $this->getSetting('paging_method');
    $placeholder = $this->getSetting('pagebreak');
    $max_char = $this->getSetting('character_count');
    $max_words = $this->getSetting('word_count');
    $max_blocks = $this->getSetting('block_count');
    $pager_position = $this->getSetting('pager_position');
    $pager_id = $this->getSetting('pager_id');

    // Make code more robust by ensuring a placeholder is set no matter what.
    if (empty($placeholder)) {
      $placeholder = "<!--pagebreak-->";
    }

    /*
    // Decided implementing a nopaging request parameter is too complicated
    // due to caching.

    // Get page request query parameters.
    $request = $this->requestStack->getCurrentRequest();

    // Get nopaging request flag.
    $nopaging = $request->get('nopaging');
    if (!empty($nopaging)) {
    $paging_method = 'BYPASS';
    }
     */

    // Initialise page processing variables.
    $total_pages = 0;
    $subpage_index = 0;
    $char_count = 0;
    $word_count = 0;
    $block_count = 0;

    foreach ($items as $delta => $item) {

      // If pager is set to bypass.
      if ($paging_method == 'BYPASS') {
        // Do nothing and exit as default field. No paging is desired.
        $elements[$delta] = [
          '#type' => 'processed_text',
          '#text' => $item->value,
          '#format' => $item->format,
          '#langcode' => $item->getLangcode(),
        ];
        break;
      }

      // Filter field content into the "$field_content" variable
      // for post processing by this display formatter.
      $field_content = new FilterProcessResult($item->value);

      // Split field_content with page break placeholders.
      //
      // If paging method is set to use user defined placeholders, do nothing.
      if ($paging_method != 'PLACEHOLDER') {

        // Remove user defined field paging placeholders from field content,
        // we don't need them here.
        $field_content = str_replace($placeholder, '', $field_content);

        // Split content by html tags.
        $split_tags = preg_split(
          '/<(!--.*?--|[^>]+?)>/s',
          $field_content, -1, PREG_SPLIT_DELIM_CAPTURE
        );

        $field_content = '';

        // Iterate content split by tags.
        foreach ($split_tags as $split_tags_key => $split_tags_value) {

          // If split_tags_key. (By bitwise and.)
          if ($split_tags_key & 1) {

            // Add opening or closing tag.
            $field_content .= '<' . $split_tags_value . '>';

            // If WORD_BLOCK method keep track of block level tags.
            if ($paging_method == 'WORD_BLOCK') {

              if (!isset($block_tag_value)) {

                $block_tag_value = (strpos($split_tags_value, ' ') !== FALSE)
                  ? strstr($split_tags_value, ' ', TRUE)
                : $split_tags_value;

                $block_tag_occurrences = 0;
                $block_count++;
              }
              else {

                // Keep track of additional occurrences of the block level tag.
                if (
                  ((strpos($split_tags_value, ' ') !== FALSE)
                    ? strstr($split_tags_value, ' ', TRUE)
                    : $split_tags_value
                  ) == $block_tag_value
                ) {

                  $block_tag_occurrences++;

                }
                else {

                  // If the tag is a closing tag and we have occurrences then
                  // deincrement.
                  if ($split_tags_value == '/' . $block_tag_value) {

                    if ($block_tag_occurrences) {

                      $block_tag_occurrences--;

                    }
                    else {

                      unset($block_tag_value);

                      if ($word_count >= $max_words) {
                        $field_content .= '&hellip;';
                        $field_content .= $placeholder;
                        $word_count = 0;
                      }

                      if ($block_count >= $max_blocks) {
                        $field_content .= $placeholder;
                        $block_count = 0;
                      }

                    }
                  }
                }
              }
            }
          }
          // Closing tags.
          else {
            if ($paging_method == 'CHARACTER') {

              $split_htmlcodes = preg_split(
                '/(&[a-zA-Z]+;)/s',
                $split_tags_value, -1, PREG_SPLIT_DELIM_CAPTURE
              );

              foreach ($split_htmlcodes as $split_htmlcodes_key => $split_htmlcodes_value
              ) {

                if ($split_htmlcodes_key & 1) {

                  // Count html charater as 1.
                  $char_count++;
                  $field_content .= $split_htmlcodes_value;

                }
                else {

                  $split_text = explode(' ', $split_htmlcodes_value);
                  $split_text_size = count($split_text);

                  foreach ($split_text as $text_value) {

                    $char_count += mb_strlen($text_value);
                    $field_content .= $text_value;

                    if ($split_text_size > 1) {
                      // Count the space.
                      $char_count++;
                      $field_content .= ' ';
                    }

                    if ($char_count >= $max_char) {
                      $field_content .= '&hellip;';
                      $field_content .= $placeholder;
                      $char_count = 0;
                    }

                  }
                }
              } // End foreach.
            }
            // If $paging_method != 'CHARACTER'/.
            else {

              $split_text = explode(' ', $split_tags_value);
              $split_text_size = count($split_text);

              foreach ($split_text as $text_value) {

                // Exclude whitespace.
                if (preg_match('/\S/s', $text_value)) {
                  ++$word_count;
                }

                $field_content .= $text_value;

                if ($split_text_size > 1) {
                  $field_content .= ' ';
                }

                if ($word_count >= $max_words) {
                  $field_content .= '&hellip;';
                  $field_content .= $placeholder;
                  $word_count = 0;
                }
              }
            }
          }
        }
      }

      // Ensure html tags are closed correctly before
      // pagebreak placeholder placement.
      do {

        // Look for consecutive placeholder and closing tag.
        // Eg. <!--pagebreak--></p>.
        $placeholder_regex = preg_quote($placeholder, '#');

        preg_match_all(
          "#($placeholder_regex)([ \t\r\n]*)(</[a-zA-Z]+>)#",
          $field_content, $match
        );

        if (isset($match[1][0]) && isset($match[3][0])) {

          // Move the placeholder to the end of closing tag.
          // From the above example </p><!--pagebreak-->.
          $match_regex = preg_quote($match[1][0]
              . ($match[2][0] ?? '')
              . $match[3][0], '#'
          );

          $field_content = preg_replace(
            "#$match_regex#",
            ($match[2][0] ?? '') . $match[3][0] . $match[1][0],
            $field_content
          );
        }

      } while (!empty($match[0])
        && !empty($match[1])
        && !empty($match[2])
        && !empty($match[3])
      );

      // Break HTML field_content properly and insert placeholders.
      $field_content = field_longtext_pager_insert_pagebreak_placeholders(
        $field_content, $placeholder);

      // Check if last page is an empty tag.
      $pagebreak = array_filter(explode($placeholder, $field_content));
      $pagebreak_end = count($pagebreak) - 1;
      $last_page = '';

      if (isset($pagebreak[$pagebreak_end])) {
        $last_page = strip_tags($pagebreak[$pagebreak_end]);
      }

      if (empty($last_page)) {
        // Remove the last page with only an empty tag field_content.
        unset($pagebreak[$pagebreak_end]);
      }

      $field_content = implode($placeholder, $pagebreak);

      if (empty($pagebreak)) {
        // Break up the field content using user defined placeholders.
        $pagebreak = array_filter(explode($placeholder, $field_content));
      }

      $pagebreak_count = count($pagebreak);

      // If pagebreaks resulted, output current subpage with pager.
      if ($pagebreak_count > 1) {

        // Record total number of pages.
        $total_pages += count($pagebreak);

        // Initialize the new pager.
        if (empty($pager_id)) {
          $pager_id = 0;
        }

        $pager_manager = $this->pagerManager->createPager($total_pages, 1, $pager_id);

        // Build pager markup.
        $pager = [
          '#type' => 'pager',
          '#element' => $pager_id,
        ];

        // Fetch subpage content.
        $subpage_index = $pager_manager->getCurrentPage();
        $subpage_content = $pagebreak[$subpage_index];

        // Build subpage field.
        $subpage_content_field =
        "<div class='field field-subpage-content subpage-content'>"
          . $subpage_content
          . "</div>";

        // Build page index and count field markup.
        $index_position = $this->getSetting('index_position');

        if ($index_position != 'HIDDEN') {

          $page_index_and_count =
            $this->t('@index_prefix @page_number of @total_pages @index_postfix', [
              '@index_prefix' => $this->getSetting('index_prefix'),
              '@page_number' => $subpage_index + 1,
              '@total_pages' => $total_pages,
              '@index_postfix' => $this->getSetting('index_postfix'),
            ]);

          $page_index_and_count_field =
            "<div class='field page-index-and-count'>"
            . $page_index_and_count
            . "</div>";
        }

        // Build estimated read time field markup.
        $read_time_position = $this->getSetting(
          'read_time_position');

        if ($read_time_position !== 'HIDDEN') {
          $average_read_time =
          field_longtext_pager_calc_average_read_time($field_content);

          if ($average_read_time <= 1) {
            $average_read_time = "<1";
          }

          $average_read_time = $this->t("Average reading time: @time.",
            ['@time' => $average_read_time]
          );

          $average_read_time_field =
            "<div class='field average-read-time'>$average_read_time</div>";
        }

        // Start building render array.
        $render = [];

        // Add to render array average read time field in field header.
        if ($read_time_position == 'ABOVE' || $read_time_position == 'BOTH') {
          $render[] = [
            '#markup' => $average_read_time_field,
          ];
        }

        // Add to render array page index and count field in field header.
        if ($index_position == 'ABOVE' || $index_position == 'BOTH') {
          $render[] = [
            '#markup' => $page_index_and_count_field,
          ];
        }

        // Add to render array pager in field header.
        if ($pager_position == 'ABOVE' || $pager_position == 'BOTH') {
          $render[] = $pager;
        }

        // Add to render array the current field content subpage.
        $render[] = [
          '#type' => 'processed_text',
          '#text' => $subpage_content_field,
          '#format' => $item->format,
          '#langcode' => $item->getLangcode(),
        ];

        // Add to render array page index and count field in field footer.
        if ($index_position == 'BELOW' || $index_position == 'BOTH') {
          $render[] = [
            '#markup' => $page_index_and_count_field,
          ];
        }

        // Add to render array pager in field footer.
        if ($pager_position == 'BELOW' || $pager_position == 'BOTH') {
          $render[] = $pager;
        }

        // Add to render array average read time field in field footer.
        if ($read_time_position == 'BELOW' || $read_time_position == 'BOTH') {
          $render[] = [
            '#markup' => $average_read_time_field,
          ];
        }

        // Build the final render array.
        $elements[$delta] = [
          '#attributes' => [
            'data-pager' => $pager_id,
          ],
        ] + $render;

        // Include CSS decorations from library.
        $elements[$delta]['#attached']['library'][]
          = 'field_longtext_pager/pager_style';

        // Add ajax pager attribute if set to on.
        $ajax_pager = $this->getSetting('ajax_pager');
        if ($ajax_pager == 1) {
          $elements[$delta]['#attributes'] = ['data-pager' => $pager_id];
        }
      }
      else {

        // Do nothing. Pager threshold settings not reached.
        $elements[$delta] = [
          '#type' => 'processed_text',
          '#text' => $item->value,
          '#format' => $item->format,
          '#langcode' => $item->getLangcode(),
        ];
      }
    }

    return $elements;
  }

}
