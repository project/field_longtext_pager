Field longtext pager
====================

Description
-----------

This module provides a field formatter for splitting large longtext field content into more readable indexed sub-pages as defined by pagebreaks or a number of characters, words or HTML blocks.

It also provides additional optional display features:
* Pager index field
* Estimated reading time field
* Ajax paging

Requirements
------------
Drupal 8.x or above.

Installation
------------
Install as you would normally install a contributed Drupal module.
Visit [https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules](https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-module) for further information.

Instructions
------------
To set module default settings visit:
SITE-URL/admin/config/content/field\_longtext\_pager

You must ensure that your text formats and editors allow your page break placeholder.
Default is: 
    <!--pagebreak-->
There are CKEditor modules to enabled manual pagebreak insertion.

To enable paging on your field visit the Manage display options in your entity
field settings, ie.:
/admin/structure/types/manage/ENTITY-ID/display

Select the "Field pager" format option and choose your desired settings.

Ajax pager results currently use the "Full page" view to return results, falling back
to the "Default" view when not found. Which is often the case with entities such as comments.

The pager ID's are automatically incremented from the setting defined in the field
options when multiple entities using paged fields are displayed on the same page.
Reserve your highest pager ID settings for your field in this scenario.

Recommended modules
-------------------
* [Ajax comments](https://www.drupal.org/project/ajax_comments)
* [Comment ajax pager](https://www.drupal.org/project/ajax_comments)
* [Pagerer](https://www.drupal.org/project/pagerer)

Technical description
---------------------
The algorithms are designed to split complicated HTML markup smartly ie.:

    <div class="text-important">
      <p class="info">
      Long content paragraph... 
    <!--pagebreak-->
      Continuation of long content paragraph.
      </p>
      <ul class="text-note">
        <li>List #1</li>
        <li>Long list #2...
     <!--pagebreak-->
        Continuation of long list #2</li>
        <li>List #3</li>
      </ul>
    </div>
    
...into this:
    
    <div class="text-important">
      <p class="info">
      Long content paragraph... 
      </p>
    </div>
    ------------Page 1------------
    <div class="text-important">
      <p class="info">
      Continuation of long content paragraph.
      </p>
      <ul class="text-note">
        <li>List #1</li>
        <li>Long list #2...</li>
      </ul>
    </div>
    ------------Page 2------------
    <div class="text-important">
      <ul class="text-note">
        <li>Continuation of long list #2</li>
        <li>List #3</li>
      </ul>
    </div>
    ------------Page 3------------

Support
-------
Please use the issue queue for filing bugs with this module at:
[http://drupal.org/project/issues/field_longtext_pager](http://drupal.org/project/issues/field_longtext_pager)

Author and maintainer
---------------------
- Dan Greenman, [Greenman IT](https://greenman-it.pw)
