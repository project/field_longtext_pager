<?php

/**
 * @file
 * Adds paging capabilities to long text fields in Drupal 8.
 * @author Dan Greenman.
 *
 * Inspiration and algorithms adapted from Drupal 7 smart_paging module.
 * Credit and gratitude to:
 * @author Roland Michael dela Peña.
 */

use Drupal\Component\Utility\Html;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\views\Views;


/**
 * Implements hook_help().
 */
function field_longtext_pager_help(
  $route_name,
  RouteMatchInterface $route_match
) {
  switch ($route_name) {
    case 'help.page.field_longtext_pager':

      // Load Readme mark down.
      $text = file_get_contents(__DIR__ . '/README.md');

      if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
        return '<pre>' . $text . '</pre>';
      }
      else {
        // Use the Markdown filter to render the README.
        $filter_manager = \Drupal::service('plugin.manager.filter');
        $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        return $filter->process($text, 'en');
      }

  }
  return NULL;
}

/**
 * Provide unique pager id for multiple field displays on the same page.
 *
 * @return
 *   The number of pager id
 */
function field_longtext_pager_get_pager_id($entity_type, $bundle, $field_name) {
  static $pager_id;

  if (!isset($pager_id)) {
    // Load the field longtext pager display settings helper service.
    $field_settings_helper = \Drupal::service(
      'field_longtext_pager.field_settings_helper');

    $pager_id = $field_settings_helper->getPagerId($entity_type, $bundle, $field_name);
  }
  else {
    $pager_id++;
  }

  return $pager_id;
}

/**
 * Implements hook_entity_display_build_alter().
 *
 * Detects pager fields and adds attributes to markup to enable Ajax paging.
 */
function field_longtext_pager_entity_display_build_alter(&$build, $context) {
  $view_mode = $context['view_mode'];

  foreach ($build as $field_name => $field) {

    // If a paged field is detected, modify field for ajax paging.
    if (!empty($field['#field_type'])
      && isset($field['#formatter'])
      && $field['#formatter'] === 'longtext_pager'
    ) {

      // Fetch the entity settings.
      $entity_type = $context['entity']->getEntityTypeId();
      $entity_id = $context['entity']->id();
      $entity = $context['entity'];
      $bundle = $context['entity']->bundle();

      // Load the field longtext pager display settings helper service.
      $field_settings_helper = \Drupal::service(
        'field_longtext_pager.field_settings_helper');

      // Get pager_id. Automatically generate unique ID's for multiple fields.
      $pager_id = field_longtext_pager_get_pager_id($entity_type, $bundle, $field_name);

      // Get the ajax enabled setting for this field display.
      $ajax_enabled = $field_settings_helper->ajaxEnabled(
        $entity_type, $bundle, $field_name, $view_mode
      );

      if ($ajax_enabled) {

        // Add data-ajax_pager entity id attribute to field.
        $build[$field_name]['#attributes']['data-ajax_pager']
          = $pager_id;

        // Add javascript library to field.
        $field_name = $field['#field_name'];
        $build[$field_name]['#attached']['library'][] =
        'field_longtext_pager/ajax_pager.js';

        // Define ajax pager api URL.
        $ajax_url = Url::fromRoute('field_longtext_pager.ajax_pager.api')->toString();

        // Attach drupalSettings for this field pager to javascript.
        $build[$field_name]['#attached']['drupalSettings']['ajax_pager'][$pager_id]
          = [
            'ajax_url' => $ajax_url,
            'ajax_pager' => [
              'pager_id' => $pager_id,
              'entity_id' => $entity_id,
              'field_name' => $field['#field_name'],
              'entity_type' => $field['#entity_type'],
              'bundle' => $field['#bundle'],
              'view_mode' => $view_mode,
            ],
          ];
      }
    }
  }
}

/**
 * Break up html content with page break placeholders.
 *
 * @param string $markup
 *   HTML content.
 * @param string $placeholder
 *   Placeholder string to be inserted between subpages.
 *
 * @return string
 *   HTML output with page break placeholders added.
 */
function field_longtext_pager_insert_pagebreak_placeholders(
  $markup,
  $placeholder = NULL
) {

  // Retrieve default pagebreak placeholder if one was not set.
  if ( empty($placeholder) ) {

    $placeholder = \Drupal::config('field_longtext_pager.settings')
      ->get('pagebreak');

  }

  // Sanitize HTML markup.
  $markup = Html::normalize($markup);

  $markup = str_replace($placeholder, '<field_longtext_pager_placeholder>', $markup)
    . '<field_longtext_pager_placeholder>';

  $struct = field_longtext_pager_pair_tags($markup);
  $split = $struct['split'];
  $tag_pairs_map = $struct['tag_pairs_map'];
  $break_positions = array_keys($tag_pairs_map['field_longtext_pager_placeholder']);
  unset($tag_pairs_map['field_longtext_pager_placeholder']);

  $break_map = [];
  foreach ( $break_positions as $break_index => $break_end ) {

    if ( $break_index ) {

      $break_start = $break_positions[$break_index - 1];
      $break_map[$break_end] = array_slice(
      $split, $break_start, ($break_end - $break_start), TRUE);

      // Remove the placeholder from the structure.
      unset($break_map[$break_end][$break_start]);

    }
    else {

      $break_start = $break_index;
      $break_map[$break_end] = array_slice(
      $split, $break_start, ($break_end - $break_start), TRUE);

    }

    foreach ($tag_pairs_map as $tag_name => $map) {
      foreach ($map as $open_tag_pos => $close_tag_pos) {

        if ( (
						$open_tag_pos < $break_end
						&& $close_tag_pos > $break_end

					) || (

						$open_tag_pos < $break_start
						&& $close_tag_pos > $break_start

					) || (

						$open_tag_pos > $break_start
						&& $close_tag_pos < $break_end
						&& $close_tag_pos !== NULL

					) || (

						$open_tag_pos > $break_start
						&& $open_tag_pos < $break_end
						&& $close_tag_pos === NULL

				) ) {

          if (empty($close_tag_pos)) {

            $break_map[$break_end][$open_tag_pos] = '<'
							. $split[$open_tag_pos] . '>';

          } else {

            $break_map[$break_end][$open_tag_pos]  = '<'
							. $split[$open_tag_pos] . '>';

            $break_map[$break_end][$close_tag_pos] = '<'
							. $split[$close_tag_pos] . '>';

          }

        }
      }
    }

    ksort($break_map[$break_end]);

    if (isset($output)) {
      $output .= $placeholder . implode('', $break_map[$break_end]);
    }
    else {
      $output = implode('', $break_map[$break_end]);
    }

  }
  return $output;
}

/**
 * Create page content HTML paired tags structure.
 *
 * Helper function to create a structured HTML paired tags mappings.
 *
 * @param string $text
 *   HTML text content.
 *
 * @return array
 *   An associative array containing:
 *   - split: array of split HTML tag(s) from text.
 *   - tag_pairs_map: Array of structured HTML paired tags mapping:
 *   $array[<tag name>][<open tag position>] = <close tag position>
 */
function field_longtext_pager_pair_tags($text) {

  $tag_pairs_map = [];

  // Split tags from text.
  $split = preg_split('/<(\/?[a-z0-9_-]+(?: [^>]*)?|!--.*?--)>/is', $text,
    -1, PREG_SPLIT_DELIM_CAPTURE);

  // Note: PHP ensures the array consists of alternating delimiters and literals
  // and begins and ends with a literal (inserting NULL as required).
  foreach ($split as $position => $value) {

    // Tags are in array's odd number index.
    if ($position & 1) {
      list($tagname) = explode(' ', $value);

      if ($tagname[0] == '/') {
        $tagname = strtolower(substr($tagname, 1));
        end($tag_pairs_map[$tagname]);

        // Its open tag pair is the last item in the array with empty value.
        while ($tag_value = current($tag_pairs_map[$tagname])) {
          prev($tag_pairs_map[$tagname]);
        }

        $pair_pos = key($tag_pairs_map[$tagname]);

        // Save position of closing tag to its pair open tag.
        $tag_pairs_map[$tagname][$pair_pos] = $position;
      }
      else {

        $tagname = strtolower($tagname);
        // Save position of open tag. For now, closing tag is unidentified.
        $tag_pairs_map[$tagname][$position] = NULL;

      }
    }
  }
  return [
    'split' => $split,
    'tag_pairs_map' => $tag_pairs_map,
  ];
}

/**
 * Returns pager field position options relative to field output.
 *
 * @return array
 *   An associative array containing pager display position options.
 */
function field_longtext_pager_position_options() {
  return [
    'BOTH' => 'Above and below',
    'ABOVE' => 'Above',
    'BELOW' => 'Below',
    'HIDDEN' => 'Hidden',
  ];
}

/**
 * Returns pager method options for choosing the paging algorithm.
 *
 * @return array
 *   An associative array containing pager display position options.
 */
function field_longtext_pager_method_options() {
  return [
    'BYPASS' => t('Bypass field pager formatter'),
    'PLACEHOLDER' => t('Manual placement of page break placeholder'),
    'CHARACTER' => t('Automatic page break by character limit'),
    'WORD' => t('Automatic page break by word limit'),
    'WORD_BLOCK' => t('Automatic page break by block word limit'),
  ];
}

/**
 * Estimages the average reading time of text input.
 *
 * @return string
 *   A string with estimated read time statistics.
 */
function field_longtext_pager_calc_average_read_time(String $text, $wpm = 225) {

  if (!empty($text)) {
    // Strip html tags from field_content and count the words.
    $field_word_count = str_word_count(strip_tags($text));

    // Calculate estimated time to read, rounding up.
    $minutes_to_read = ceil(($field_word_count / $wpm));
    $hours_to_read = floor($minutes_to_read / 60);

    // Convert minutes to remainder minutes using modulus 60.
    $minutes_to_read = $minutes_to_read % 60;

    // Hour output: singular.
    if ($hours_to_read == 1) {
      $hours_to_read = t("@hours hour", [
        '@hours' => $hours_to_read,
      ]);
    }
    // Hour output: plural.
    if ($hours_to_read > 1) {
      $hours_to_read = t("@hours hours", [
        '@hours' => $hours_to_read,
      ]);
    }
    // Zero hour output.
    if ($hours_to_read <= 0) {
      $hours_to_read = '';
    }

    if ($minutes_to_read == 1) {
      // Minute output: singular.
      $label = 'minute';
    } else {
      // Minute output: plural.
      $label = 'minutes';
    }

    $minutes_to_read = t("@minutes @label", [
      '@minutes' => $minutes_to_read,
      '@label' => $label,
    ]);

    return ("$hours_to_read $minutes_to_read");
  }
}

// Canonical stuff. Still in development
function canonical() {
   // Get current node ID and canonical URL from route.
    $node = $this->routeMatch->getParameter('node');

    if ($node instanceof NodeInterface) {
      $nid = $node->id();

      // TODO: Dont use string and render. Use a link render array.
      $canonical_url = Url::fromRoute(
        'entity.node.canonical',
        ['node' => $nid],
        ['absolute' => TRUE]
      )->toString();

      // Create nopager url.
      $nopager_url = "$canonical_url?nopager=1";
    }

 // Get current node ID and canonical URL from route.
        if ($node instanceof NodeInterface) {
          $nid = $node->id();

          // Get canonical url.
          $canonical_url = Url::fromRoute(
            'entity.node.canonical',
            ['node' => $nid],
            ['absolute' => TRUE]
          );

          // TODO.
          $canonical_url = $canonical_url->toString();

          $nopager_url = "$canonical_url?nopager=1";
        }
      // Alter prev, next and canonical URLs to paged links.
        $prev_subpage_index = $subpage_index - 1;
        $next_subpage_index = $subpage_index + 1;

        if ($prev_subpage_index > 0) {
          // Initialise prev variable by copying current variable.
          $prev_get_request_index = $page_get_request_index;

          // Change current pager requested page index to
          // point at previous subpage.
          $prev_get_request_index[$pager_id] = $prev_subpage_index;

          // Flatten array into a string.
          $prev_get_request = implode(',', $prev_get_request_index);

          // Build link render array.
          $prev = [
            '#type' => 'html_tag',
            '#tag' => 'link',
            '#attributes' => [
              'rel' => 'prev',
              'href' => "$canonical_url?page=$prev_get_request",
            ],
          ];

          // Add prev header link for previous subpage.
          $elements['#attached']['html_head'][] = [$prev, 'field_longtext_pager_prev'];
        }

       if ($subpage_index != 0) {
          // Set fullpage canonical link for all subpage locations.
          $canonical_link_tag = [
            '#type' => 'html_tag',
            '#tag' => 'link',
            '#attributes' => [
              'rel' => 'canonical',
              'href' => "$canonical_url?page=$page_get_request",
            ],
          ];
          $elements['#attached']['html_head'][] = [$canonical_link_tag, 'canonical'];
        }

       // Add header link to next subpage.
        if ($next_subpage_index <= $total_pages) {

          // Initialise prev variable by copying current variable.
          $next_get_request_index = $page_get_request_index;

          // Change current pager requested page index to point
          // at previous subpage.
          $next_get_request_index[$pager_id] = $next_subpage_index;

          // Flatten array into a string.
          $next_get_request = implode(',', $next_get_request_index);

          $next = [
            '#type' => 'html_tag',
            '#tag' => 'link',
            '#attributes' => [
              'rel' => 'next',
              'href' => "$canonical_url?page=$next_get_request",
            ],
          ];
          $elements['#attached']['html_head'][] = [$next, 'field_longtext_pager_next'];
        }
}
